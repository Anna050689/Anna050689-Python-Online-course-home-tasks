def binary_repr(number: int) -> (str, int):
    list_num = []

    while number > 0:
        remainder = number % 2
        list_num.append(remainder)
        number = number // 2
    list_num.reverse()
    binary_repr = ''.join(map(str, list_num))
    total = sum(list_num)
    return binary_repr, total

if __name__ == '__main__':
    number = int(input("Enter your number:"))
    binary_repr, total = binary_repr(number)
    print(f'Number is {number}, binary represantation is {binary_repr}, \
    sum is {total}')
