# This program is implemented by using loop

def factorial(number: int):
    num = 1
    for i in range(1, number + 1):
        num *= i
    return num

if __name__ == '__main__':
    number = int(input("Please enter the number: "))
    if number == 0:
        print('Factorial is 1')
    elif number < 1:
        print("Factorial does not exist for negative numbers")
    else:
        print(f'Factorial is {factorial(number)}')

# This program is implemented by using recursion

def factorial(number: int):
    if number == 1:
        return number
    else:
        return factorial(number - 1) * number

if __name__ == '__main__':
    number = int(input("Please enter the number: "))
    if number == 0:
        print('Factorial is 1')
    elif number < 1:
        print("Factorial does not exist for negative numbers")
    else:
        print(f'Factorial is {factorial(number)}')




