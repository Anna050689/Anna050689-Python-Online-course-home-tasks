# Hometask 4

### Task 1. Transpose a matrix
In this task I should write [**the program**](./transpose.py) that can transpose any matrix.<br/>
**Note**: The transpose of a matrix is an operator which flips a matrix over its diagonal;
that is, it switches the row and column indices of the matrix **A** by producing another matrix.<br/>
**Note**: You could describe a matrix in Python using nested lists, *e.g matrix = [[0, 1, 2], [3, 4, 5]]*

### Task 2. Factorial.
In this task I should write [**the program**](./factorial.py) which calculates the factorial for positive 
number *n*.<br/>
**Note**: The factorial of a positive integer *n* is the product of 
multiplication of all 
positive integers less than or equal to *n*.

### Task 3. Fibonacci sequence
In this task I should write [**the program**](./fib_sequence.py) which calculates 
the result value for a positive integer *n*, which is equal to the sum of the first 
*n* Fibonacci numbers.<br/>
**Note**: Fibonacci numbers are a series of numbers in which each next number is equal to the sum of 
the two preceding ones.
 
### Task 4. Binary represantation
In this task I should write [**the program**](./binary_repr.py) which for a positive integer *n* 
calculates the result value, which is equal to the sum of the **1**
in binary representation of *n*.<br/>
**Note**: Do not use any format functions.

### Prerequisites

1. Windows or Unix based OS
2. Pre-installed Python 3.6 or higher

### How to run this it?

&nbsp;1. Checkout the project. Please, move to [**program transpose_matrix**](./transpose.py) from task 1 or to 
[**program get_factorial**](./factorial.py) from task 2 or to [**program get_fibonacci_sequence**](./fib_sequence.py) 
from task 3 or to [**program get_binary_representation](./binary_repr.py) 

&nbsp;2. From root directory run command.<br/>
Use command to run program transpose_matrix:

```bash
    python ./transpose.py
 ```
Use command to run program get_factorial:
```bash
    python ./factorial.py
```
Use command to run program get_fibonacci_sequence:
```bash
    python ./fib_sequence.py
```
Use command to run program get_binary_representation:
```bash
    python ./binary_repr.py
```
&nbsp;3. If you prefer IDE, please, import this project or simply run Python file in your favourite IDE.
