def transpose(matrix: list) -> list:
    transpose_matrix = []

    for j in range(len(matrix[0])):
        transpose_matrix.append([])
        for i in range(len(matrix)):
            transpose_matrix[j].append(matrix[i][j])
    print(transpose_matrix)


if __name__ == '__main__':
    matrix = [[1, 2], [3, 4], [5, 6]]
    transpose(matrix)

# This variant let us generate the matrix and then transpose it.

def generate(rows, columns) -> list:
    matrix = []
    for i in range(rows):
        matrix.append([])
        for j in range(columns):
            elem = int(input(f"Enter {j} number of {i} row:"))
            matrix[i].append(elem)
    return matrix


def transpose(matrix: list) -> list:
    transpose_matrix = []

    for j in range(len(matrix[0])):
        transpose_matrix.append([])
        for i in range(len(matrix)):
            transpose_matrix[j].append(matrix[i][j])
    print(transpose_matrix)

if __name__ == '__main__':
    rows = int(input("Enter the count of rows of this matrix:"))
    columns = int(input("Enter the count of columns of this matrix:"))
    transpose(generate(rows, columns))



