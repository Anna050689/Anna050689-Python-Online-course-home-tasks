# This program is implemented by using loop
def get_fib(number: int):
    if number == 1:
        numbers = [0]
        total = sum(numbers)
        return numbers, total
    elif number == 2:
        numbers = [0, 1]
        total = sum(numbers)
        return numbers, total
    elif number == 3:
        numbers = [0, 1, 1]
        total = sum(numbers)
        return numbers, total
    elif number > 3:
        numbers = [0, 1, 1]
        for i in range(3, number):
            numbers.append(numbers[-1] + numbers[-2])
        total = sum(numbers)
        return numbers, total


if __name__ == '__main__':
    length = int(input("Please, enter the length of sequence:"))
    if length <= 0:
        print("Please enter the number greater than 0")
    else:
        print(f'Fibonacci sequence is {get_fib(length)[0]}')
        print(f'Sum of elements in Fibonacci sequence is {get_fib(length)[1]}')

# This program is implemented by using recursion

def get_fib(number: int):
    if number <= 1:
        return number
    else:
        return (get_fib(number - 1) + get_fib(number - 2))


if __name__ == '__main__':
    length = int(input("Please, enter the length of sequence:"))
    if length <= 0:
        print("Please enter the positive number")
    else:
        numbers = []
        for number in range(length):
            numbers.append(get_fib(number))
        total = sum(numbers)
        print(f'Fibonacci sequence is {numbers}')
        print(f'Sum of elements in Fibonacci sequence is {total}')


