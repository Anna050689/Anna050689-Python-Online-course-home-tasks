from math import sqrt

a = float(input("Enter the length of side a:"))
b = float(input("Enter the length of side b:"))
c = float(input("Enter the length of side c:"))


def get_area(a, b, c):
    s = (a + b + c) / 2
    area = sqrt(s * (s - a) * (s - b) * (s - c))
    return round(area, 2)

print("The area of this triangle is", get_area(a, b, c))
