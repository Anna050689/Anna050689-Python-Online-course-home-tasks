# Hometask 1
### PEP-8
I got acquainted and read the documentation 
about code convention **PEP-8** according [this link](https://www.python.org/dev/peps/pep-0008/ (https://pep8.org/)).
### Calculate the area of triangle

The goals of this task are next:

- Calculate the area of triangle with sides a = 4.5, b = 5.9 and c = 9;
- Print calculated value with precision = 2

There is a formula to find the area of any triangle
when we know the lengths of all three of its sides.<br/>
It is called **Heron's formula**.

![Heron's formula](https://andymath.com/wp-content/uploads/2019/08/heronnotes.jpg)

In my program I create a function which uses Heron's formula
to execute area of triangle.

In order to execute calculations I import **sqrt()** function from standart 
library [**math**](https://docs.python.org/3/library/math.html). 
>*math.sqrt(x)*<br/>
>Return the square root of x.

In order to round the result to a specified precision I use built-in function [**round()**](https://docs.python.org/3/library/functions.html).
>*round*(number[, ndigits])<br/>
>Return number rounded to ndigits precision after the decimal point. If ndigits is omitted or is None, 
>it returns the nearest integer to its input.

### Prerequisites

1. Windows or Unix based OS
2. Pre-installed Python 2.6 or higher 

### How to run this it?

&nbsp;1. Checkout the project. Please, move to [this link](./get_area.py). 

&nbsp;2. From root directory run command and follow the instructions (program will suggest you to enter some values).

```bash
    python ./get_area.py
 ```

&nbsp;3. If you prefer IDE, please, import this project or simply run Python file in your favourite IDE.


Fox example, if you enter such lengths of triangle as a = 4.5, b = 5.9, c = 9 you get next result:

![result](result.png)



