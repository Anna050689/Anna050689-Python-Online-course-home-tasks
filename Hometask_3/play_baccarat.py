card_scores = {
    '10': 0,
    'J': 0,
    'Q': 0,
    'K': 0,
    'A': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9
}


def play_baccarat(card: str) -> int:
    if card not in card_scores.keys():
        print('Do not cheat!')
    elif card in card_scores.keys():
        return card_scores[card]


if __name__ == '__main__':
total = 0
card = input('Play first card:')
total += play_baccarat(card)
card = input('Play second card:')
total += play_baccarat(card)
if total > 9:
    total -= 10
    print(f'Your result: {total}')
else:
    print('Stop game!')
