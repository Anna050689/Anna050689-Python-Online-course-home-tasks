def fizz_buzz(number: int):
    if number % 15 == 0:
        print("FizzBuzz")
    elif number % 3 == 0:
        print("Fizz")
    elif number % 5 == 0:
        print("Buzz")
    else:
        print('This number is out of categories')

if __name__ == '__main__':
    number = int(input('Enter the number:'))
    if number >= 1 and number <= 100:
        fizz_buzz(number)
    else:
        print(f'{number} is out of range [1, 100]')
