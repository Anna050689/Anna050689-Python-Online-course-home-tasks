# Hometask 3
### Task 1
In [**task 1**](./validate_point.py) I have to write a program that determines whether the Point A(x, y) is in the shaded area or not.
![picture_to_task1](./picture_to_task1.png)

In [**task 2**](./fizz_buzz.py) I have to write a program that prints the input number from 1 to 100.
But for multiple of three print "Fizz" instead of the number and for the multiples of five print "Buzz".
For numbers which are multiples of both three and five print "FizzBuzz".

In [**task 3**](./play_baccarat.py) I have to write a program which simulates the one round of play for baccarat game.

*Rules*:
- cards have a point value:
    * the 2 through 9 cards in each suit are worth face value (in points);
    * the 10, jack, queen, and king have no point value (i.e. are worth zero);
    * aces are worth 1 point
- Sum the values of cards. If total is more than 9 reduce 10 from result.
E.g *5 + 9 = 14, 14 - 10 = 4, 4 is a result*.
- Player is not allowed to play another cards  like *joker*.

### Prerequisites

1. Windows or Unix based OS
2. Pre-installed Python 3.6 or higher

### How to run this it?

&nbsp;1. Checkout the project. Please, move to [**program validate_point**](./validate_point.py) from task 1 or to 
[**program fizz_buzz**](./fizz_buzz.py) from task 2 or to [**program play_baccarat**](./play_baccarat.py) 
from task 3. 

&nbsp;2. From root directory run command.<br/>
Use command to run program validate_point:

```bash
    python ./validate_point.py
 ```
Use command to run program fizz_buzz:
```bash
    python ./fizz_buzz.py
```
Use command to run program play_baccarat:
```bash
    python ./play_baccarat.py
```
&nbsp;3. If you prefer IDE, please, import this project or simply run Python file in your favourite IDE.


