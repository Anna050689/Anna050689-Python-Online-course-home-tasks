def validate_point(x: float, y: float):
    if y >= 0 and y <= 1:
        return y >= abs(x)
    return False

if __name__ == '__main__':
    x = float(input('Enter x: '))
    y = float(input('Enter y: '))

    print('Is point in shadow area:', validate_point(x, y))
