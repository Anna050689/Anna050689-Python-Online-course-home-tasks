# Python Online Course 2020
In this repository I demonstrate my hometasks in Python Online Course 2020.

### Hometask 1
1. Read the documentation about code convention **PEP-8**.
2. Write a program which can calculate the area of triangle with sides a = 4.5, b = 5.9 and c = 9 and print calculated value with precision = 2.

### Hometask 2
In [**exercise 1**](./Hometask_2/count_negatives.py) I have to write a program which returns the count of negative numbers in next list [4, -9, 8, -11, 8].<br/>
**Note**: do not use conditionals or loops.

In [**exercise 2**](./Hometask_2/change_positions.py) I have 5 best tennis players according APT rankings. Set the first-place player 
(at the front of the list) to last place and vice versa.

In [**exercise 3**](./Hometask_2/swap_words.py) I have to swap words "reasonable" and "unreasonable" in quote 
*"The reasonable man adapts himself to the world; the unreasonable one persists in 
trying to adapt the world to himself."<br/>
**Note**: Do not use <string>.replace() function or similar.

### Hometask 3
In [**task 1**](./Hometask_3/validate_point.py) I have to write a program that determines whether the Point A(x, y) is in the shaded area or not.

In [**task 2**](./Hometask_3/fizz_buzz.py) I have to write a program that prints the input number from 1 to 100.
But for multiple of three print "Fizz" instead of the number and for the multiples of five print "Buzz".
For numbers which are multiples of both three and five print "FizzBuzz".

In [**task 3**](./Hometask_3/play_baccarat.py) I have to write a program which simulates the one round of play for baccarat game.

*Rules*:
* cards have a point value:
    * the 2 through 9 cards in each suit are worth face value (in points);
    * the 10, jack, quenn, and king have no point value (i.e. are worth zero);
    * aces are worth 1 point
- Sum the values of cards. If total is more than 9 reduce 10 from result.
E.g, *5 + 9 = 14, 14 - 10 = 4, 4 is a result*.
- Player is not allowed to play another cards  like *joker*.
### Hometask 4

In [**task 1**](./Hometask_4/transpose.py) I should write the program that can transpose any matrix.<br/>
**Note**: The transpose of a matrix is an operator which flips a matrix over its diagonal;
that is, it switches the row and column indices of the matrix **A** by producing another matrix.<br/>
**Note**: You could describe a matrix in Python using nested lists, *e.g matrix = [[0, 1, 2], [3, 4, 5]]*

In [**this task**](./Hometask_4/factorial.py) I should write the program which calculates the factorial for positive 
number *n*.<br/>
**Note**: The factorial of a positive integer *n* is the product of 
multiplication of all 
positive integers less than or equal to *n*.

In [**this task**](./Hometask_4/fib_sequence.py) I should write the program which calculates 
the result value for a positive integer *n*, which is equal to the sum of the first 
*n* Fibonacci numbers.<br/>
**Note**: Fibonacci numbers are a series of numbers in which each next number is equal to the sum of 
the two preceding ones.
 
In [**this task**](./Hometask_4/binary_repr.py) I should write the program which for a positive integer *n* 
calculates the result value, which is equal to the sum of the **1**
in binary representation of *n*.<br/>
**Note**: Do not use any format functions.

### Hometask 5

In [**this task**](./Hometask_5/is_sorted.py) I should create function [**is_sorted**](./Hometask_5/is_sorted.py), determining whether 
a given **array** of integer values of arbitrary length is sorted 
in a given **order**(the order is set up by enum value **SortOrder**). 
Array and sort order are passed by parameters. Function does not change the array.

In [**this task**](./Hometask_5/transform.py) I should create function [**transform**](./Hometask_5/transform.py), replacing the value of each element 
of an integer **array** with the sum of this element value and its index, only if the given **array** 
is sorted in the given **order**(the order is set up by enum value **SortOrder**). 
Array and sort order are passed by parameters. To check, if the array is sorted, the function 
**is_sorted** from the task 1 is called.

In [**this task**](./Hometask_5/mult_arithmetic_elements.py) I should create function [**mult_arithmetic_elements**](./Hometask_5/mult_arithmetic_elements.py), which 
determines the multiplication of the first **n** elements of arithmetic progression of real numbers with a given 
initial element of progression **a<sub>1</sub>** and progression step **t**.<br/>
**a<sub>n</sub>** is calculated by the formula **a<sub>n+1</sub> = a<sub>n</sub> + t**.

In [**this task**](./Hometask_5/sum_geometric_elements.py) I should create function [**sum_geometric_elements**](./Hometask_5/sum_geometric_elements.py), determining the sum of the 
first elements of a decreasing geometric progression of real numbers with a given initial 
element of a progression **a<sub>1</sub>** and a given progression step **t**, while the last element must 
be greater than a given **alim**.<br/>
**a<sub>n</sub>** is calculated by the formula **a<sub>n+1</sub> = a<sub>n</sub> * t**, 0<t<1.

# Hometask 6
In task 1 I should implement a [**function**](./Hometask_6/combine_dict.py), that receives changeable 
number of dictionaries (keys - letters, values - numbers) and combines 
them into one dictionary.
Dict values should be summarized in case of identical keys.

In task 2 I should create generic type [**CustomList**](./Hometask_6/custom_list.py) - the list of values, 
which has length that is extended when new elements are added to the list.<br/>
**CustomList** is a collection — the list of values of random type, its size changes 
dynamically and there is a possibility to index list elements. Indexation in
the list starts with 0.<br/>
Values of random type can be located in the list, it should be created empty and 
a set of original values should be specified. List length changes while
adding and removing elements. The elements can be added and removed using specific methods. 
List can be checked whether there is a predetermined value in the list.<br/>
List indexing allows to perform the following operations based on indexes:
* To change and read values of existing elements by using indexer
* To receive index of predetermined value
* To remove value based on index

The list can be cleared, its length can be identified, 
its elements can be received in the form of linked list Item that is available via link to head.
Collection CustomList can be used in operator foreach and other constructions that are oriented 
to the presence of numerator in class.

The task has two levels of complexity: Low and Advanced.

Low level tasks require implementation of the following functionality:
* Creating of empty user list and the one based on elements set 
(the elements are stored in CustomList in form of unidirectional linked list
* Adding, removing elements
* Operations with elements by index
* Clearing the list, receiving its length
* Receiving link to linked elements list

Advanced level tasks require implementation of the following functionality:
* All completed tasks of Low level
* Generating exceptions, specified in xml-comments to class methods
* Receiving from numerator list for operator foreach

# Hometask 7

In task 1 I should develop [**program**](./Hometask_7/rectangle.py) which implements classes **Rectangle** and **ArrayRectangles** 
with a predefined functionality.<br/>
On a **Low level** it is obligatory:<br/>
To develop **Rectangle** class with following content:<br/>
2 closed float **sideA** and **sideB** (sides A and B of the rectangle)<br/>
Constructor with two parameters **a** and **b** (parameters specify rectangle sides)<br/>
Constructor with a parameter **a**< (parameter specify side A of a rectangle, side B is always equal to 5)<br/>
Method **GetSideA**, returning value of the side A<br/>
Method **GetSideB**, returning value of the side B<br/>
Method **Area**, calculating and returning the area value<br/>
Method **Perimeter**, calculating and returning the perimeter value<br/>
Method **IsSquare**, checking whether current rectangle is shape square or not. 
Returns true if the shape is square and false in another case.<br/>
Method **ReplaceSides**, swapping rectangle sides<br/>

On **Advanced level** also needed:<br/>
Complete Level **Low** Assignment<br/>
Develop class **ArrayRectangles**, in which declare:
Private field **rectangle_array** - array of rectangles<br/>
Constructor creating an empty array of rectangles with length **n**<br/>
Constructor that receives an arbitrary amount of objects of type **Rectangle** or an array of objects of type **Rectangle**.<br/>
Method **AddRectangle** that adds a rectangle of type **Rectangle** to the array on the nearest free place and returning true, or returning false, if there is
no free space in the array<br/>
Method **NumberMaxArea**, that returns order number(index) of the rectangle with the maximum area value(numeration starts from zero)<br/>
Method **NumberMinPerimeter**, that returns order number(index) of the rectangle with the minimum perimeter value(numeration starts from zero)<br/>
Method **NumberSquare**, that returns the number of squares in the array of rectangles

### Task 2
In this task I should develop [**program**](./Hometask_7/employee.py) which implements classes **Employee**, **SalesPerson**, **Manager** and **Company** with predefined functionality.<br/>
**Low** level requires:<br/>
To create basic class **Employee** and declare following content:<br/>
Three closed fields - text field **name**(employee last name), money fields - **salary**, **bonus**<br/>
Public property **Name** for reading employee's last name<br/>
Public property **Salary** for reading and recording salary field<br/>
Constructor with parameters string **name** and money **salary**(last name and salary are set)<br/>
Method **SetBonus** that sets bonuses to salary, amount of which is delegeted/conveyed as bonus<br/>
Method **ToPay** that returns the value of summarized salary and bonus<br/>
To create class **SalesPerson** as class **Employee** inheritor and declare with it:<br/>
Closed integer field **percent**(percent of sales targets plan performance/execution)<br/>
Constructor with parameters: **name**-employee last name, **salary**, **percent** - percent of plan performance, first 
two of which are passed to basic class constructor<br/>
Redefine method of parent class **SetBonus** in the following way:
if the sales person completed the plan more then 100%, so his bonus is doubled(is multiplied by 2), 
and if more than 200% -  bonus is tripled(is multiplied by 3)<br/>
To create class **Manager** as **Employee** class inheritor, and declare with it:<br/>
Closed integer field **quantity**(number of clients, who were served by the manager during a month)<br/>
Constructor with parameters: **name**-employee last name, **salary**, **percent** - percent of plan performance, first 
two of which are passed to basic class constructor<br/>
Redefine method of parent class **SetBonus** in the following way:
if the manager served over 100 clients, his bonus is increased by 500, and if more than 150 clients - by 1000<br/>

**Advanced** level requires:<br/>
To fully complete **Low** level tasks.<br/>
Create class **Company** and declare within it:<br/>
Constructor that receives employee array of **Employee** type with arbitrary length<br/>
Method **GiveEverebodyBonus** with money parameter **companyBonus** that sets the amount of basic bonus of each employee.<br/>
Method **TotalToPay** that returns total amount of salary of all employees including awarded bonus.<br/>
Method **NameMaxSalary** that returns employee last name, who receives maximum salary including bonus. 

# Hometask 9

### Task 1
In this task I should implement a [**function**](./Hometask_9/replace_quotes.py) which receives a string and 
replaces all `"` symbols with `'` and vise versa.

### Task 2
In this task I should write a [**function**](Hometask_9/palindrome.py) that check whether a string 
is a palindrome or not.

### Task 3
In this task I should implement a [**function**](Hometask_9/get_shortest_word.py) which returns the shortest word 
in the given string. The word can contain any symbols except whitespaces(' ', \n, \t 
and so on). If there are multiple shortest words in the string with a same length 
return the word that occurs first. Usage of any split functions is forbidden.

### Task 4
In this task I should implement a [**bunch of functions**](./Hometask_9/get_char.py) which receive a changeable number 
of strings and return next parameters:<br/>
1) characters that appear in all strings<br/>
2) characters that appear in at least one string<br/>
3) characters that appear in at least two strings<br/>
4) characters of alphabet, that were not used in any string<br/>
**Note**: use *string.ascii_lowercase* for list of alphabet letters

### Task 5
In this task I should implement a [**function**](Hometask_9/count_letters.py), that takes string as an argument and returns 
a dictionary, that contains letters of given string as keys and a number of their occurence 
as values. 

# Hometask 10

### Task 1
In this task I should write a [**program**](Hometask_10/sorted_names.py) which open file [**'data/unsorted_names.txt'**](./data/unsorted_names.txt) in data folder. Sort the names and write them to a 
new file called [**'sorted_names.txt'**](Hometask_10/data/sorted_names.txt). Each name should starts with a new line as in the 
lexicographical order.

### Task 2
In this task I should implement a [**function**](Hometask_10/most_common_words.py) which search for most common words in the file.
In this task I use [**'data/lorem_ipsum.txt'**](Hometask_10/data/lorem_ipsum.txt) file as an example.

### Task 3
File [**'data/students.csv'**](Hometask_10/data/students.csv) stores information about students in csv format.<br/>
This file contains the student's names, age and average mark.<br/>
1) In this task I should implement a [**function**](Hometask_10/get_top_performers.py) which receives file path and returns names of top performer 
students.<br/>
2) I also should implement a [**function**](Hometask_10/set_desc_age.py) which receives the file path with students info and 
writes CSV student information to the new file in descending order of age.

# Hometask 11

### Task 1
Look through file [**modules/legb.py.**](Hometask_11/modules/legb.py)<br/>
1) Find a way to call inner_function without moving it from inside of enclosed_function.<br/>
2) Modify ONE LINE in inner_function to make it print variable 'a' from global scope.<br/>
3) Modify ONE LINE in inner_function to make it print variable 'a' from enclosing function.

The execution of this task you can find in [**this file**](./call_inner_func.py).

### Task 2

Implement a decorator [**remember_result**](Hometask_11/remember_result.py) which remembers last result of function it decorates and 
prints it before next call.

### Task 3

Implement a decorator [**call_once**](Hometask_11/call_once.py) which runs a function or method once and caches the result. 
All consecutive calls to this function should return cached result no matter the arguments.

### Task 4

Run the module [**modules/mod_a.py**](Hometask_11/modules/mod_a.py). Check its result. Explain why does this happen. Try to change x to a list[1,2,3]. 
Explain the result. Try to change import to from x import * where x - module names. Explain the result.<br/>

**Explanation**: Objects in the module are only accessible when prefixed with <module_name> via dot notation. In [**mod_a.py**](Hometask_11/modules/mod_a.py) I imported 
[**mod_c.py**](Hometask_11/modules/mod_c.py) and [**mod_b.py**](Hometask_11/modules/mod_b.py) using import statement - import <module_name>. In this case I had access only to variable **mod_c.x** from mod_b.py, variable **x** from mod_c.py wasn't available.
That's why I don't see changes in the variable **x**.<br/>
In other hand, if I try to use import statement - from <module_name> import * I had access to the variables **mod_c.x** from mod_b.py and **x** from mod_c.py. 
That's why I can see changes in the variable **x**.

# Hometask 12

### [Task1](Hometask_12)
**1a.** Invoke **pwd** to see your current working directory (there should be your home directory).

**1b.** Collect output of these commands:
* ls-l/
* ls
* ls~
* ls-l
* ls-a
* ls-la
* ls -lda ~

Note differences between produced outputs. Describe (in few words) purposes of these commands.

**1c.** Execute and describe the following commands (store the output, if any):
* mkdir test
* cd test
* pwd
* touch test.txt
* ls -l test.txt
* mkdir test2
* mv test.txt test2
* cd test2
* ls
* mv test.txt test2.txt
* ls
* cp test2.txt ..
* cd..
* ls
* rm test2.txt
* rmdir test2

**1d.** Execute and describe the difference:
* cat /etc/fstab
* less /etc/fstab
* more /etc/fstab

**1e.** Look through man pages of the listed above commands.
 
### [Task2](Hometask_12)

**2a.** Discovering soft and hard links. Comment on results of these commands (place the output into your report):
* cd
* mkdir test
* cd test
* touch test1.txt
* echo “test1.txt” > test1.txt
* ls -l . 
* ln test1.txt test2.txt
* ls -l . 
* echo “test2.txt” > test2.txt
* cat test1.txt test2.txt
* rm test1.txt
* ls-l . 
* ln -s test2.txt test3.txt
* ls -l . 
* rm test2.txt; ls -l .

**2b.** Dealing with **chmod**.
An executable script. Open your favorite editor and put these lines into a file:
```bash
#!/bin/bash
echo “Drugs are bad MKAY?”
``` 
Give name “script.sh” to the script and call to • chmod +x script.sh
Then you are ready to execute the script:<br/>
**./script.sh**
 




