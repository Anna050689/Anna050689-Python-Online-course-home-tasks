# Hometask 11
### Task 1
Look through file [**modules/legb.py.**](./modules/legb.py)<br/>
1) Find a way to call inner_function without moving it from inside of enclosed_function.<br/>
2) Modify ONE LINE in inner_function to make it print variable 'a' from global scope.<br/>
3) Modify ONE LINE in inner_function to make it print variable 'a' from enclosing function.

The execution of this task you can find in [**this file**](./call_inner_func.py).
### Task 2
Implement a decorator [**remember_result**](./remember_result.py) which remembers last result of function it decorates and 
prints it before next call.
### Task 3
Implement a decorator [**call_once**](./call_once.py) which runs a function or method once and caches the result. 
All consecutive calls to this function should return cached result no matter the arguments.
### Task 4
Run the module [**modules/mod_a.py**](./modules/mod_a.py). Check its result. Explain why does this happen. Try to change x to a list[1,2,3]. 
Explain the result. Try to change import to from x import * where x - module names. Explain the result.<br/>

**Explanation**: Objects in the module are only accessible when prefixed with <module_name> via dot notation. In [**mod_a.py**](./modules/mod_a.py) I imported 
[**mod_c.py**](./modules/mod_c.py) and [**mod_b.py**](./modules/mod_b.py) using import statement - import <module_name>. In this case I had access only to variable **mod_c.x** from mod_b.py, variable **x** from mod_c.py wasn't available.
That's why I don't see changes in the variable **x**.<br/>
In other hand, if I try to use import statement - from <module_name> import * I had access to the variables **mod_c.x** from mod_b.py and **x** from mod_c.py. 
That's why I can see changes in the variable **x**.


 
