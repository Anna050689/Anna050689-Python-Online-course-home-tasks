total_result = []


def call_once(func):
    def wrapper(*args):
        result = func(*args)
        global total_result
        if len(total_result) == 0:
            total_result.append(result)
        return total_result[0]
    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b

print(sum_of_numbers(13, 42))
print(sum_of_numbers(999, 100))
print(sum_of_numbers(134, 412))
print(sum_of_numbers(856, 232))
