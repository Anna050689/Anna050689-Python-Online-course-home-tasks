total_result = [None]


def remember_result(func):
    def wrapper(*args):
        global total_result
        print(f"Last result = {total_result[-1]}")
        result = func(*args)
        total_result.append(result)
    return wrapper


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
    print(f"Current result = '{result}'")
    return result

sum_list("a", "b")
sum_list("abc", "cde")
sum_list(3, 4, 5)







