from modules import legb


def call_inner_func():
    # print the value of global variable a
    from modules.legb import a
    print(a)
    # return the value of local variable a
    return legb.enclosing_function()


inner_func = call_inner_func()
inner_func()




