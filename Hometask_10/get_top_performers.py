def get_top_performers(file_path, number_of_top_students=5):
    """
    function which receives file path and
    returns names of top performer students
    """
    import pandas as pd
    import os.path

    if not os.path.exists(file_path):
        print("This file doesn't exist")
        raise FileNotFoundError

    data_of_students = pd.read_csv(file_path)
    result = data_of_students.sort_values(by='average mark', ascending=False).head(5)
    return result['student name']


if __name__ == '__main__':
    print(f"There are a names of top performer students: \n"
          f"{get_top_performers('./data/workers.csv', 6)}")

