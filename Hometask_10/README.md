# Hometask 10

### Task 1
In this task I should write a [**program**](./sorted_names.py) which open file [**'data/unsorted_names.txt'**](./data/unsorted_names.txt) in data folder. Sort the names and write them to a 
new file called [**'sorted_names.txt'**](./data/sorted_names.txt). Each name should starts with a new line as in the 
lexicographical order.

### Task 2
In this task I should implement a [**function**](./most_common_words.py) which search for most common words in the file.
In this task I use [**'data/lorem_ipsum.txt'**](./data/lorem_ipsum.txt) file as an example.

### Task 3
File [**'data/students.csv'**](./data/students.csv) stores information about students in csv format.<br/>
This file contains the student's names, age and average mark.<br/>
1) In this task I should implement a [**function**](./get_top_performers.py) which receives file path and returns names of top performer 
students.<br/>
2) I also should implement a [**function**](./set_desc_age.py) which receives the file path with students info and 
writes CSV student information to the new file in descending order of age.
