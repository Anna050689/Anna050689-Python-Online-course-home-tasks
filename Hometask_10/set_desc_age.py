def set_desc_age(file_path, new_file_path):
    """
    function which receives the file path with students info
    and writes CSV student information to the new file
    in descending order of age
    """
    import pandas as pd
    import os.path

    if not os.path.exists(file_path):
        print("This file doesn't exist")
        raise FileNotFoundError

    data_of_students = pd.read_csv(file_path)
    result = data_of_students.sort_values(by='age', ascending=False)

    result.to_csv(new_file_path)


if __name__ == '__main__':
    set_desc_age('./data/students.csv', './data/students_sorted_by_age.csv')

