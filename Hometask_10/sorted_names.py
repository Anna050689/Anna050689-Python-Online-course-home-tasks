unsorted_names = []

with open('./data/unsorted_names.txt') as f:
    for line in f.readlines():
        unsorted_names.append(line)

sorted_names = sorted(unsorted_names)

with open('./data/sorted_names.txt', 'w', encoding='utf-8') as f:
    for name in sorted_names:
        f.write(name)

with open('./data/sorted_names.txt') as f:
    data = f.readlines()
    print(data)


