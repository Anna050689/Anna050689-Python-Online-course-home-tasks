def most_common_words(file_path, number_of_words=3):
    """
    function which searches for most common words in the file
    """
    import re
    import os.path

    if not os.path.exists(file_path):
        print("This file doesn't exist")
        raise FileNotFoundError

    with open(file_path) as f:
        data = f.read().lower()

    data_list = re.split('\W+', data)

    data_dict = {}
    for value, key in enumerate(data_list):
        if data_dict.get(key) is None:
            data_dict[key] = 1
        else:
            data_dict[key] += 1

    list_dict = list(data_dict.items())
    list_dict.sort(key=lambda i: i[1], reverse=True)

    for value, number in list_dict[:number_of_words]:
        print(value)


if __name__ == '__main__':
    most_common_words('./data/lorem_ipsum.txt', 5)

