# Hometask 8
In this task I should analyse different examples of code which use exception handling.

### Example 1
```python
a = int(input("Enter the number: "))
b = 7
assert a > b, 'Not enough'
```
In this example if user enters the number less or equal to 7, the assert condition evaluates to false 
and it raises an **AssertionError** exception with an optional error message. In this case user receives
message - *'Not enough'*.

### Example 2
```python
def foo():
    try:
        bar(x, 4)
    finally:
        print('after bar')
    print('or this after bar?')

foo()
```
In this example when user call function **foo** it raises **NameError** exception 
because name **'bar'** is not defined. In the output user receives the result of **finally** 
block than sends message about **NameError**. The code in **try** clause will stop as soon as 
exception is encountered so rest code after **finally** block won't be executed.

### Example 3
```python
def baz():
    try:
        return 1
        with open("/tmp/logs.txt") as file:
            print(file.read())
            return
    finally:
        return 2

result = baz()
print(result)
``` 
In this example the output is **2**. Although in **try** block function returns 1, 
**finally** block must be executed in any way. Thus, the function returns 2 which 
assigned to variable **result**.

### Example 4
```python
def foo():
    try:
        print(1)
        return
    finally:
        print(2)
    else:
        print(3)

foo()
```
In this example user receives message about **SyntaxError** because if we use **try-finally** 
we mustn't use **else** block.

### Example 5
```python
try:
    if '1' != 1:
        raise "Error"
    else:
        print("Error has not occured")
except "Error":
    print("Error has occured")
```
In this example in the output there are two messages about **TypeError**. The first message raises because we don't send class of exceptions as 
the argument after **raise**. The second message raises because we also don't send class of exceptions as 
the argument after **except**. 

### Example 6
```python
flag = False
while not flag:
    try:
        filename = input("Enter filename: ")
        file = open(filename, 'r')
    except:
        print("Input file not found")
```
In this example we can see endless code because neither in **try** block nor in **except** block 
the value of **flag** doesn't change to True.