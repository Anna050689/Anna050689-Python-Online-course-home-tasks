class Node:
    def __init__(self, item):
        self.elem = item
        self.next = None

    def __repr__(self):
        return self.elem


class CustomeList:
    def __init__(self):
        self.__head = None

    # returns the length of list
    def length(self):
        cur = self.__head
        count = 0
        while cur is not None:
            count += 1
            cur = cur.next
        return count

    # gets the value according to index
    def get(self, index_item):
        cur = self.__head
        index = 0
        while index <= index_item:
            if index == index_item:
                return cur.elem
            index += 1
            cur = cur.next

    # gets the index of element
    def index(self, item):
        cur = self.__head
        index = 0
        while cur.elem != item:
            index += 1
            cur = cur.next
        return index

    # allows iterating through the list
    def __iter__(self):
        cur = self.__head
        while cur is not None:
            yield cur.elem
            cur = cur.next

    # adds an element at the beginning of the list
    def add(self, item):
        node = Node(item)
        node.next = self.__head
        self.__head = node

    # adds an element at the end of the list
    def append(self, item):
        node = Node(item)
        if self.__head is None:
            self.__head = node
        else:
            cur = self.__head
            while cur.next is not None:
                cur = cur.next
            cur.next = node

    # adds an element at the given position: index starts at 0
    def insert(self, index, item):
        if index <= 0:
            self.add(item)
        elif index > (self.length() - 1):
            self.append(item)
        else:
            node = Node(item)
            count = 0
            cur = self.__head
            while count < index - 1:
                count += 1
                cur = cur.next
            node.next = cur.next
            cur.next = node

    # delete element of the list
    def remove(self, item):
        cur = self.__head
        prev = None
        while cur is not None:
            if cur.elem == item:
                if prev is None:
                    self.__head = cur.next
                else:
                    prev.next = cur.next
                break
            else:
                prev = cur
                cur = cur.next

    # check whether there is a predetermined value in the list
    def check(self, item):
        cur = self.__head
        while cur is not None:
            if cur.elem == item:
                return True
            else:
                cur = cur.next
        return False

    # print the list
    def __repr__(self):
        cur = self.__head
        nodes = []
        while cur is not None:
            nodes.append(str(cur.elem))
            cur = cur.next
        return ', '.join(nodes)

    # clear the list
    def clear(self):
        self.__init__()


if __name__ == '__main__':
    li = CustomeList()
    li.append('Anna')
    li.add(78)
    li.remove('Anna')
    li.insert(0, 'Bob')
    print(li.get(1))
    print(li.index('Bob'))
    print(li)
    print(li.length())
    print(li.check('Bob'))
    print(li.check(1900))
    print(li)
    for i in li:
        print(i)







