def combine_dict(*dict):
    common_dict = {}
    for dictionary in dict:
        for key in dictionary.keys():
            try:
                common_dict[key] += dictionary[key]
            except KeyError:
                common_dict[key] = dictionary[key]
    return common_dict

if __name__ == '__main__':
    dict_1 = {'a': 100, 'b': 200, 'c': 300, 'd': 100}
    dict_2 = {'a': 100, 'b': 200, 'c': 300}
    dict_3 = {'a': 100, 'b': 200, 'c': 300}
    print(combine_dict(dict_1, dict_2, dict_3))







