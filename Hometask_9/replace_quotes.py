def replace_quotes(string: str) -> str:
    """
    function which receives a string and
    replaces all `"` symbols with `'`
    and vise versa
    """
    if '\'' in string:
        return string.replace('\'', '\"')
    elif '\"' in string:
        return string.replace('\"', '\'')


if __name__ == '__main__':
    string = input("Enter your string:")
    print(replace_quotes(string))

