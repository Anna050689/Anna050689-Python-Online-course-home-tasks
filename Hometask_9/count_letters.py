def count_letters(string: str) -> dict:
    """
    function that takes string as an argument and returns a dictionary,
    that contains letters of given string as keys and a number of their
    occurrence as values
    """
    dict = {}
    for value, key in enumerate(string):
        if dict.get(key) is None:
            dict[key] = 1
        else:
            dict[key] += 1
    return dict


if __name__ == '__main__':
    string = input("Enter the string which contains only letters:")
    if string.isalpha():
        print(count_letters(string))
    else:
        print("Your entered a wrong string.")
if __name__ == '__main__':
    string = input("Enter the string which contains only letters:")
    if string.isalpha():
        print(count_letters(string))
    else:
        print("Your entered a wrong string.")
