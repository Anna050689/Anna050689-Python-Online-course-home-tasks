import string


def get_char_1(strings):
    """
    function which returns set of characters that appear in all strings
    """
    sets = []
    for i in range(len(strings)):
        sets.append(set(strings[i].lower()))
    sets[0].intersection_update(*sets[1:])
    return sets[0]


def get_char_2(strings):
    """
    function which returns set of characters,
    that appear in at least one string
    """
    common_string = ''.join(strings)
    return set(common_string.lower())


def get_char_3(strings):
    """
    function which returns set of characters,
    that appear at least two strings
    """
    sets = []
    for i in range(len(strings)):
        sets.append(set(strings[i].lower()))

    list_common_letters = []
    for i in range(len(sets) - 1):
        result = sets[i].intersection(sets[i + 1])
        list_common_letters.append(result)

    result = set()
    for value in list_common_letters:
        result |= value

    return result


def get_char_4(strings):
    """
    function which returns characters of alphabet,
    that were not used in any string
    """
    all_letters = set(string.ascii_lowercase)
    common_string = ''.join(strings)
    string_letters = set(common_string.lower())
    return all_letters.difference(string_letters)


if __name__ == '__main__':
    strings = input("Enter your string").split()
    print(f'Set of characters that appear in all strings: '
          f'{get_char_1(strings)}')
    print(f'set of characters,that appear in at least one string: '
          f'{get_char_2(strings)}')
    print(f'set of characters, that appear at least two strings: '
          f'{get_char_3(strings)}')
    print(f'Set of characters of alphabet, that were not used in any string: '
          f'{get_char_4(strings)}')




