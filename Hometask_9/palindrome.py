def palindrome(string: str) -> bool:
    """
    function which checks whether a string
    is a palindrome or not
    """
    join_string = ''.join(string.split())
    return join_string.lower()[:] == join_string.lower()[::-1]


if __name__ == '__main__':
    string = input("Enter your string:")
    print(f'This string is palindrom - {palindrome(string)}')

