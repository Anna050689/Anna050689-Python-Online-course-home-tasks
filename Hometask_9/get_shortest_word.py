def get_shortest_word(strings: list) -> str:
    """
    function which returns the shortest word
    in the given string. The word can contain
    any symbols except whitespaces(' ', \n, \t
    and so on). If there are multiple shortest
    words in the string with a same length
    returns the word that occurs first
    """
    import re


    list_of_len = []
    for string in strings:
        if bool(re.search(r"\S", string)):
            list_of_len.append(len(string))
        else:
            print("You enter the string with whitespace")
            break
    index = list_of_len.index(min(list_of_len))
    return strings[index]


if __name__ == '__main__':
    strings = input("Enter your string:").split()
    print(get_shortest_word(strings))






