# Hometask 9

### Task 1
In this task I should implement a [**function**](./replace_quotes.py) which receives a string and 
replaces all `"` symbols with `'` and vise versa.

### Task 2
In this task I should write a [**function**](palindrome.py) that check whether a string 
is a palindrome or not.

### Task 3
In this task I should implement a [**function**](get_shortest_word.py) which returns the shortest word 
in the given string. The word can contain any symbols except whitespaces(' ', \n, \t 
and so on). If there are multiple shortest words in the string with a same length 
return the word that occurs first. Usage of any split functions is forbidden.

### Task 4
In this task I should implement a [**bunch of functions**](./get_char.py) which receive a changeable number 
of strings and return next parameters:<br/>
1) characters that appear in all strings<br/>
2) characters that appear in at least one string<br/>
3) characters that appear in at least two strings<br/>
4) characters of alphabet, that were not used in any string<br/>
**Note**: use *string.ascii_lowercase* for list of alphabet letters

### Task 5
In this task I should implement a [**function**](./count_letters.py), that takes string as an argument and returns 
a dictionary, that contains letters of given string as keys and a number of their occurrence. 
as values. 