def mult_arithmetic_elements(number: int, a_1: float, t: float) -> float:
    result = 1
    for i in range(1, number + 1):
        result *= a_1 + (i - 1) * t
    return result


if __name__ == '__main__':
    number = int(input('Please enter the count of elements '
                       'of arithmetic progression:'))
    a_1 = float(input('Please enter the initial element of progression:'))
    t = float(input('Please enter the progression step:'))
    print(f'Multiplication elements of arithmetic progression '
          f'equals {mult_arithmetic_elements(number, a_1, t)}')


# This function is implemented by using a generator

def mult_arithmetic_elements(number: int, a_1: float, t: float) -> float:
    for i in range(1, number + 1):
        yield a_1 + (i - 1) * t


if __name__ == '__main__':
    number = int(input('Please enter the count of elements '
                       'of arithmetic progression:'))
    a_1 = float(input('Please enter the initial element of progression:'))
    t = float(input('Please enter the progression step:'))
    gen = mult_arithmetic_elements(number, a_1, t)
    result = 1
    for item in gen:
        result *= item
    print(f'Multiplication of the first {number} elements '
          f'of this arithmetic progression equals {result}')

















