def sum_geometric_elements(a_1: float, t: float, alim: float) -> float:
    number = a_1
    total = 0
    while number > alim:
        total += number
        number *= t
    return total


if __name__ == '__main__':
    a_1 = float(input("Please, enter the initial element of progression:"))
    t = float(input("Please, enter the step of progression "
                    "which value is from 0 to 1:"))
    alim = float(input("Please, enter the number which will be less "
                       "than the last element of progression:"))
    if t <= 0 or t >= 1:
        print('You entered wrong value of the step progression')
    else:
        print(f"For a progression, where initial element is {a_1}, "
              f"step is {t}, sum of the first elements, greater than {alim} "
              f"equals to {sum_geometric_elements(a_1, t, alim)}.")

# This function is implemented by using a generator

def sum_geometric_elements(a_1: float, t: float, alim: float) -> float:
    number = a_1
    while number > alim:
        yield number
        number *= t

if __name__ == '__main__':
    a_1 = float(input("Please, enter the initial element of progression:"))
    t = float(input("Please, enter the step of progression "
                    "which value is from 0 to 1:"))
    alim = float(input("Please, enter the number which will be less "
                       "than the last element of progression:"))
    if t <= 0 or t >= 1:
        print('You entered wrong value of the step progression')
    else:
        gen = sum_geometric_elements(a_1, t, alim)
        total = 0
        for item in gen:
            total += item
        print(f"For a progression, where initial element is {a_1}, "
              f"step is {t}, sum of the first elements, greater than {alim} "
              f"equals to {total}.")


