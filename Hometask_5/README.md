# Hometask 5
### Task 1
In this task I should create function [**is_sorted**](./is_sorted.py), determining whether 
a given **array** of integer values of arbitrary length is sorted 
in a given **order**(the order is set up by enum value **SortOrder**). 
Array and sort order are passed by parameters. Function does not change the array.
### Task 2
In this task I should create function [**transform**](./transform.py), replacing the value of each element 
of an integer **array** with the sum of this element value and its index, only if the given **array** 
is sorted in the given **order**(the order is set up by enum value **SortOrder**). 
Array and sort order are passed by parameters. To check, if the array is sorted, the function 
**is_sorted** from the task 1 is called.
### Task 3
In this task I should create function [**mult_arithmetic_elements**](./mult_arithmetic_elements.py), which 
determines the multiplication of the first **n** elements of arithmetic progression of real numbers with a given 
initial element of progression **a<sub>1</sub>** and progression step **t**.<br/>
**a<sub>n</sub>** is calculated by the formula **a<sub>n+1</sub> = a<sub>n</sub> + t**.
### Task 4
In this task I should create function [**sum_geometric_elements**](./sum_geometric_elements.py), determining the sum of the 
first elements of a decreasing geometric progression of real numbers with a given initial 
element of a progression **a<sub>1</sub>** and a given progression step **t**, while the last element must 
be greater than a given **alim**.<br/>
**a<sub>n</sub>** is calculated by the formula **a<sub>n+1</sub> = a<sub>n</sub> * t**, 0<t<1.
### Prerequisites

1. Windows or Unix based OS
2. Pre-installed Python 3.6 or higher

### How to run this it?

&nbsp;1. Checkout the project. Please, move to [**program is_sorted**](./is_sorted.py) from task 1 or to 
[**program transform**](./transform.py) from task 2 or to [**program mult_arithmetic_elements**](./mult_arithmetic_elements.py) 
from task 3 or to [**program sum_geometric_elements**](./sum_geometric_elements.py) 

&nbsp;2. From root directory run command.<br/>
Use command to run program **is_sorted**:

```bash
    python ./is_sorted.py
 ```
Use command to run program **transform**:
```bash
    python ./transform.py
```
Use command to run program **mult_arithmetic_elements**:
```bash
    python ./mult_arithmetic_elements.py
```
Use command to run program **sum_geometric_elements**:
```bash
    python ./sum_geometric_elements.py
```
&nbsp;3. If you prefer IDE, please, import this project or simply run Python file in your favourite IDE.
