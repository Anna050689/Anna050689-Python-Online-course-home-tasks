from enum import Enum


class SortOrder(Enum):
    ASCENDING = 0
    DESCENDING = 1


def is_sorted(array: list, order: SortOrder):
    if order == SortOrder.ASCENDING.value:
        if all(array[i] <= array[i+1] for i in range(len(array) - 1)):
            return True
        elif all(array[i] >= array[i+1] for i in range(len(array) - 1)):
            return False
        return False
    elif order == SortOrder.DESCENDING.value:
        if all([array[i] >= array[i+1] for i in range(len(array) - 1)]):
            return True
        elif all([array[i] <= array[i+1] for i in range(len(array) - 1)]):
            return False
        return False


if __name__ == '__main__':
    list_ = list(map(int, input('Please enter numbers of array:').split()))
    order_ = int(input("Please enter 0 if you want to check whether "
                       "list in ascending order or 1 to check whether list "
                       "in descending order:"))
    if order_ not in [SortOrder.ASCENDING.value, SortOrder.DESCENDING.value]:
        order_ = int(input('Please, enter 0 or 1 as the order:'))
    if is_sorted(list_, order_):
        print("This array is sorted in given order")
    else:
        print("This array isn't sorted in given order")
