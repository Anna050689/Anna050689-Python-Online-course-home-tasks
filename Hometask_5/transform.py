from enum import Enum
from is_sorted import is_sorted


class SortOrder(Enum):
    ASCENDING = 0
    DESCENDING = 1


def transform(array: list, order: SortOrder):
    if is_sorted(array, order):
        list_ = []
        for i in range(len(array)):
            list_.append(i + array[i])
        return list_
    return "This array didn't sort in given order"

if __name__ == '__main__':
    array_ = list(map(int, input('Please enter numbers of array:').split()))
    order_ = int(input('Please enter 0 if this list is in ascending order '
                       'or 1 if this list is in descending order:'))
    print(transform(array_, order_))

# This function is implemented by using a generator
from enum import Enum
from is_sorted import is_sorted


class SortOrder(Enum):
    ASCENDING = 0
    DESCENDING = 1


def transform(array: list, order: SortOrder):
    for i in range(len(array)):
        yield array[i] + i


if __name__ == '__main__':
    array_ = list(map(int, input('Please enter numbers of array:').split()))
    order_ = int(input('Please enter 0 if this list is in ascending order '
                       'or 1 if this list is in descending order:'))
    if is_sorted(array_, order_):
        gen = transform(array_, order_)
        transform_list = []
        for item in gen:
            transform_list.append(item)
        print(transform_list)
    else:
        print("This list isn't sorted in given order.")


