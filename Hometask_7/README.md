# Hometask 7
### Task 1
In this task I should develop [**program**](./rectangle.py) which implements classes **Rectangle** and **ArrayRectangles** 
with a predefined functionality.<br/>
On a **Low level** it is obligatory:<br/>
To develop **Rectangle** class with following content:<br/>
2 closed float **sideA** and **sideB** (sides A and B of the rectangle)<br/>
Constructor with two parameters **a** and **b** (parameters specify rectangle sides)<br/>
Constructor with a parameter **a**< (parameter specify side A of a rectangle, side B is always equal to 5)<br/>
Method **GetSideA**, returning value of the side A<br/>
Method **GetSideB**, returning value of the side B<br/>
Method **Area**, calculating and returning the area value<br/>
Method **Perimeter**, calculating and returning the perimeter value<br/>
Method **IsSquare**, checking whether current rectangle is shape square or not. 
Returns true if the shape is square and false in another case.<br/>
Method **ReplaceSides**, swapping rectangle sides<br/>

On **Advanced level** also needed:<br/>
Complete Level **Low** Assignment<br/>
Develop class **ArrayRectangles**, in which declare:
Private field **rectangle_array** - array of rectangles<br/>
Constructor creating an empty array of rectangles with length **n**<br/>
Constructor that receives an arbitrary amount of objects of type **Rectangle** or an array of objects of type **Rectangle**.<br/>
Method **AddRectangle** that adds a rectangle of type **Rectangle** to the array on the nearest free place and returning true, or returning false, if there is
no free space in the array<br/>
Method **NumberMaxArea**, that returns order number(index) of the rectangle with the maximum area value(numeration starts from zero)<br/>
Method **NumberMinPerimeter**, that returns order number(index) of the rectangle with the minimum perimeter value(numeration starts from zero)<br/>
Method **NumberSquare**, that returns the number of squares in the array of rectangles

### Task 2
In this task I should develop [**program**](./employee.py) which implements classes **Employee**, **SalesPerson**, **Manager** and **Company** with predefined functionality.<br/>
**Low** level requires:<br/>
To create basic class **Employee** and declare following content:<br/>
Three closed fields - text field **name**(employee last name), money fields - **salary**, **bonus**<br/>
Public property **Name** for reading employee's last name<br/>
Public property **Salary** for reading and recording salary field<br/>
Constructor with parameters string **name** and money **salary**(last name and salary are set)<br/>
Method **SetBonus** that sets bonuses to salary, amount of which is delegeted/conveyed as bonus<br/>
Method **ToPay** that returns the value of summarized salary and bonus<br/>
To create class **SalesPerson** as class **Employee** inheritor and declare with it:<br/>
Closed integer field **percent**(percent of sales targets plan performance/execution)<br/>
Constructor with parameters: **name**-employee last name, **salary**, **percent** - percent of plan performance, first 
two of which are passed to basic class constructor<br/>
Redefine method of parent class **SetBonus** in the following way:
if the sales person completed the plan more then 100%, so his bonus is doubled(is multiplied by 2), 
and if more than 200% -  bonus is tripled(is multiplied by 3)<br/>
To create class **Manager** as **Employee** class inheritor, and declare with it:<br/>
Closed integer field **quantity**(number of clients, who were served by the manager during a month)<br/>
Constructor with parameters: **name**-employee last name, **salary**, **percent** - percent of plan performance, first 
two of which are passed to basic class constructor<br/>
Redefine method of parent class **SetBonus** in the following way:
if the manager served over 100 clients, his bonus is increased by 500, and if more than 150 clients - by 1000<br/>

**Advanced** level requires:<br/>
To fully complete **Low** level tasks.<br/>
Create class **Company** and declare within it:<br/>
Constructor that receives employee array of **Employee** type with arbitrary length<br/>
Method **GiveEverebodyBonus** with money parameter **companyBonus** that sets the amount of basic bonus of each employee.<br/>
Method **TotalToPay** that returns total amount of salary of all employees including awarded bonus.<br/>
Method **NameMaxSalary** that returns employee last name, who receives maximum salary including bonus. 


