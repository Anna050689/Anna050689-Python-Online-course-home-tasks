class Employee:
    def __init__(self, last_name, salary):
        self.__name = last_name
        self.__salary = salary
        self.__bonus = 0

    def set_bonus(self, bonus):
        self.__bonus = bonus

    @property
    def name(self):
        return self.__name

    @property
    def salary(self):
        return self.__salary

    @salary.setter
    def salary(self, salary):
        self.__salary = salary

    @property
    def bonus(self):
        return self.__bonus

    def to_pay(self):
        return self.__salary + self.__bonus


class SalesPerson(Employee):
    def __init__(self, name, salary, percent):
        self.__percent = percent
        self.__bonus = 0

        Employee.__init__(self, name, salary)

    def set_bonus(self, bonus):
        if self.__percent > 200:
            self.__bonus = 3 * bonus
        elif self.__percent > 100:
            self.__bonus = 2 * bonus
        else:
            self.__bonus = bonus


class Manager(Employee):
    def __init__(self, name, salary, client_amount):
        self.__quantity = client_amount
        self.__bonus = 0

        Employee.__init__(self, name, salary)

    def set_bonus(self, bonus):
        if self.__quantity > 150:
            self.__bonus = 1000 * bonus
        elif self.__quantity > 100:
            self.__bonus = 500 * bonus
        else:
            self.__bonus = bonus


class Company:

    def __init__(self, *employees):
        self.company = []
        for employee in employees:
            self.company.append(employee)
        self.count_employees = len(self.company)

    def give_everybody_bonus(self, company_bonus):
        for i in range(self.count_employees):
            self.company[i].set_bonus(company_bonus)

    def total_to_pay(self):
        total_to_pay = []
        for i in range(self.count_employees):
            total_to_pay.append(self.company[i].to_pay())
        return sum(total_to_pay)

    def name_max_salary(self):
        list_salary = sorted(self.company, key=lambda c: c.to_pay())
        return self.company[-1].name


if __name__ == '__main__':
    employee_1 = Employee('Ivanov', 10000)
    employee_2 = Employee('Petrov', 10500)
    employee_1.set_bonus(100)
    employee_2.set_bonus(150)
    print(f'The last name of first employee is {employee_1.name}. '
          f'His salary equals {employee_1.salary}.')
    print(f'The last name of second employee is {employee_2.name}. '
          f'His salary equals {employee_2.salary}.')
    employee_1.salary = 11000
    employee_2.salary = 12000
    print(f'The salary of the first employee has been increased. '
          f'Now his salary equals {employee_1.salary}.')
    print(f'The salary of the second employee has been increased. '
          f'Now his salary equals {employee_2.salary}.')
    print(f'The first employee earned a salary with a bonus '
          f'in the amount of {employee_1.to_pay()}.')
    print(f'The second employee earned a salary with a bonus '
          f'in the amount of {employee_2.to_pay()}.')
    sales_person_1 = SalesPerson('Dobrinin', 15000, 80)
    sales_person_2 = SalesPerson('Michin', 16000, 120)
    sales_person_1.set_bonus(300)
    sales_person_2.set_bonus(300)
    print(f'The last name of first sales person is {sales_person_1.name}. '
          f'His salary equals {sales_person_1.salary}.')
    print(f'The last name of second sales person is {sales_person_2.name}. '
          f'His salary equals {sales_person_2.salary}.')
    employee_1.salary = 16000
    employee_2.salary = 18000
    print(f'The salary of the first sales person has been increased. '
          f'Now his salary equals {sales_person_1.salary}.')
    print(f'The salary of the second sales person has been increased. '
          f'Now his salary equals {sales_person_2.salary}.')
    print(f'The first sales person earned a salary with a bonus '
          f'in the amount of {sales_person_1.to_pay()}.')
    print(f'The second sales person earned a salary with a bonus '
          f'in the amount of {sales_person_2.to_pay()}.')
    manager_1 = Manager('Grigoriev', 18000, 400)
    manager_2 = Manager('Saveliev', 19000, 560)
    manager_1.set_bonus(500)
    manager_2.set_bonus(500)
    print(f'The last name of first manager is {manager_1.name}. '
          f'His salary equals {manager_1.salary}.')
    print(f'The last name of second manager is {manager_2.name}. '
          f'His salary equals {manager_2.salary}.')
    manager_1.salary = 20000
    manager_2.salary = 20500
    print(f'The salary of the first manager has been increased. '
          f'Now his salary equals {manager_1.salary}.')
    print(f'The salary of the second manager has been increased. '
          f'Now his salary equals {manager_2.salary}.')
    print(f'The first manager earned a salary with a bonus '
          f'in the amount of {manager_1.to_pay()}.')
    print(f'The second manager earned a salary with a bonus '
          f'in the amount of {manager_2.to_pay()}.')
    MyCompany = Company(employee_1, employee_2, sales_person_1,
                        sales_person_2, manager_1, manager_2)
    print(f'The bonus of first employee was {employee_1.bonus}.')
    MyCompany.give_everybody_bonus(200)
    print(f'The company established basic bonus for every employee.')
    print(f'Now the bonus of second employee is {employee_1.bonus}.')
    print(f'The total amount of salary of all employees including '
          f'awarded bonus is {MyCompany.total_to_pay()}.')
    print(f'The employee who received maximum salary including bonus '
          f'is {MyCompany.name_max_salary()}.')







