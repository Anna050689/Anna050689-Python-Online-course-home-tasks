class Rectangle:

    def __init__(self, a, b=5):
        self.__side_a = float(a)
        self.__side_b = float(b)

    def area(self):
        return self.__side_a * self.__side_b

    def perimeter(self):
        self.perimeter = 2 * (self.__side_a + self.__side_b)
        return self.perimeter

    def is_square(self):
        return self.__side_a == self.__side_b

    def replace_sides(self):
        self.__side_a, self.__side_b = self.__side_b, self.__side_a

    @property
    def a(self):
        return self.__side_a

    @property
    def b(self):
        return self.__side_b


class ArrayRectangle:

    def __init__(self, n):
        self.__rectangle_array = []
        self.size = 0
        self.n = n

    def add_rectangle(self, rect):
        if isinstance(rect, Rectangle):
            if self.size < self.n:
                self.__rectangle_array.append(rect)
                self.size += 1
                return True
            else:
                return False

    def number_max_area(self):
        list_area = []
        for i in range(len(self.__rectangle_array)):
            list_area.append(self.__rectangle_array[i].area())
        return list_area.index(max(list_area))

    def number_min_perimeter(self):
        list_perimeter = []
        for i in range(len(self.__rectangle_array)):
            list_perimeter.append(self.__rectangle_array[i].perimeter)
        return list_perimeter.index(min(list_perimeter))

    def number_of_squares(self):
        list_is_square = []
        for i in range(len(self.__rectangle_array)):
            list_is_square.append(self.__rectangle_array[i].is_square())
        return sum(list_is_square)


if __name__ == '__main__':
    rectangle = Rectangle(10)
    print(f'The side a of first rectangle equals {rectangle.a}')
    print(f'The side b of first rectangle equals {rectangle.b}')
    rectangle.replace_sides()
    print(f'The side a of this rectangle after replacing sides '
          f'equals {rectangle.a}')
    print(f'The side b of this rectangle after replacing sides '
          f'equals {rectangle.b}')

    rectangle_2 = Rectangle(5)
    print(f'The side a of second rectangle equals {rectangle_2.a}')
    print(f'The side b of second rectangle equals {rectangle_2.b}')
    print(f'It is square - {rectangle_2.is_square()}')

    rectangle_3 = Rectangle(15, 10)
    print(f'The side a of third rectangle equals {rectangle_3.a}')
    print(f'The side b of third rectangle equals {rectangle_3.b}')

    rectangle_4 = Rectangle(15)
    print(f'The side a of third rectangle equals {rectangle_4.a}')
    print(f'The side b of third rectangle equals {rectangle_4.b}')

    array = ArrayRectangle(10)
    print('Adding instances to the array of rectangles')
    array.add_rectangle(rectangle)
    array.add_rectangle(rectangle_2)
    array.add_rectangle(rectangle_3)
    array.add_rectangle(rectangle_4)

    print(f'The perimeter of first rectangle equals '
          f'{rectangle.perimeter()}')
    print(f'The perimeter of second rectangle equals '
          f'{rectangle_2.perimeter()}')
    print(f'The perimeter of third rectangle equals '
          f'{rectangle_3.perimeter()}')
    print(f'The perimeter of fourth rectangle equals '
          f'{rectangle_4.perimeter()}')
    print(f'The index of the rectangle with the minimum '
          f'perimeter value is {array.number_min_perimeter()}')
    print(f'The area of first rectangle equals {rectangle.area()}')
    print(f'The area of second rectangle equals {rectangle_2.area()}')
    print(f'The area of third rectangle equals {rectangle_3.area()}')
    print(f'The area of fourth rectangle equals {rectangle_4.area()}')

    print(f'The index of the rectangle with the maximum area value is '
          f'{array.number_max_area()}')
    print(f'The index of the rectangle with the minimum perimeter is '
          f'{array.number_min_perimeter()}')
    print(f'The number of squares is {array.number_of_squares()}')










