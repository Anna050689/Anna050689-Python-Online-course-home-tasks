# Hometask 2
### Exercise 1
The goal of exercise 1 is to write a program which returns the count of negative numbers in next list [4, -9, 8, -11, 8].<br/>
**Note**: do not use conditionals or loops.

In [**this program**](./count_negatives.py) I create function which receives list of numbers and returns the length of list of negatives numbers from received list. 
The list of negatives numbers are received as a result of *filter()* function.

I also create [**program**](./count_negatives_gen.py) in which you can enter your list in console and program returns the count of negative numbers in your list.

### Exercise 2
In exercise 2 I have 5 best tennis players according APT rankings. I have to set the first-place player 
(at the front of the list) to last place and vice versa.

In [**this program**](./change_positions.py) I create function which receives list and sets the first element of list to last place and vice versa 
by using positional assignment like *list[0], list[-1] = list[-1], list[0]*.

### Exercise 3
In exercise 3 I have to swap words "reasonable" and "unreasonable" in quote 
*"The reasonable man adapts himself to the world; the unreasonable one persists in 
trying to adapt the world to himself."<br/>
**Note**: Do not use <string>.replace() function or similar.

In [**this program**](./swap_words.py) I create a function which receives string and returns the modified string 
with words swapped. In order to swap words I use slice notation.
### Prerequisites

1. Windows or Unix based OS
2. Pre-installed Python 3.6 or higher
### How to run this it?

&nbsp;1. Checkout the project. Please, move to [**program count_negatives**](./count_negatives.py) from exercise 1 or to 
[**program change_positions**](./change_positions.py) from exercise 2 or to [**program swap_words**](./swap_words.py) 
from exercise 3. 

&nbsp;2. From root directory run command.<br/>
Use command to run program count_negatives:

```bash
    python ./count_negatives.py
 ```
Use command to run program change_positions:
```bash
    python ./change_positions.py
```
Use command to run program swap_words:
```bash
    python ./swap_words.py
```
&nbsp;3. If you prefer IDE, please, import this project or simply run Python file in your favourite IDE.



