def count_negatives(numbers: list) -> int:
    return len(list(filter(lambda x: x < 0, numbers)))

if __name__ == '__main__':
    input_list = input("Enter numbers of your list, separated by whitespace:")
    input_numbers = list(map(int, input_list.split()))
    print(f'There are {count_negatives(input_numbers)} negative numbers in list {input_numbers}')