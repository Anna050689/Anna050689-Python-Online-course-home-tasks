def swap_words(quote: str):
    words = quote.split()
    print(' '.join([words[0]] + [words[9]] + words[2:9] +
                   [words[1]] + words[10:]))

if __name__ == '__main__':
    swap_words(
        quote='The reasonable man adapts himself to the world; \
        the unreasonable one persists in trying to adapt the world to himself.'
    )




