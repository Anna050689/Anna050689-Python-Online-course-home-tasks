# Hometask 12

### Task 1
**1a.** Invoke **pwd** to see your current working directory (there should be your home directory).<br>
![pwd](images_task1/pwd.png)

**1b.** Collect output of these commands:<br/>
* ls-l/
* ls
* ls~
* ls-l
* ls-a
* ls-la
* ls -lda ~

Note differences between produced outputs. Describe (in few words) purposes of these commands.

**ls** command is used to list information about the files(the current directory by 
default). Sort entries alphabetically if none of -cftuvSUX nor --sort is specified.
**ls** with no option lists files and directories in bare format where we won’t be able to view details like file types, size, modified date 
and time, permission and links etc.

![ls](images_task1/ls.png)

**ls -l /** command is used to list the contents of the current directory 
in a table format with columns including:
* content permissions
* number of links to the content
* owner of the content
* group owner of the content
* size of the content in bytes
* last modified date / time of the content
* file or directory name

![ls -l /](images_task1/ls-l.png)

ls ~ command is used to list the contents in the users's home directory.

![ls ~](images_task1/ls~.png)

**ls -l** command is used to list the files in the working directory in long listing format.

![ls -l](images_task1/ls-l.png)

**ls -a** command is used to list the files in the working directory without ignoring 
entries starting with dot(hiding files).

![ls -a](images_task1/ls-a.png)

**ls -la** command is used to list the files in the working directory without ignoring 
entries starting with dot(hiding files) in long listing format.

![ls -la](images_task1/ls-la.png)

ls -lda ~ command is used to list directories themselves, not their contents in the working directory without ignoring 
entries starting with dot(hiding files) in long listing format.

![ls -lda](images_task1/ls-lda.png)

**1c.** Execute and describe the following commands (store the output, if any):
* mkdir test
* cd test
* pwd
* touch text.txt
* ls -l text.txt
* mkdir text2
* mv text.txt text2
* cd text2
* ls
* mv text.txt text2.txt
* ls
* cp text2.txt ..
* cd..
* ls
* rm text2.txt
* rmdir text2

**mkdir** command is used to create directories.

![mkdir](images_task1/mkdir_1.png)

![mkdir](images_task1/mkdir_2.png)

**cd test** command is used to change the working directory. To do this, type **cd** followed by the pathname of the desired 
working directory. Pathnames can be specified two different ways; absolute pathnames or relative pathnames. In this case, I 
set as a the working directory the directory *test*.

![cd test](images_task1/cd.png)

**pwd** command is used to see the name of the working directory. In this way, I can check that the working directory is *test*.

![pwd](images_task1/pwd_2.png)

**touch text.txt** command is used to create a file *text.txt* without any content. 
The file created using touch command is empty. 
This command can be used when the user doesn’t have data to store at 
the time of file creation.

![touch](images_task1/touch.png)

**ls -l text.txt** command is used to list the files in the working directory in long listing format. In this way, I can 
check that the file *text.txt* exists in directory *test*.

![ls -l text.txt](images_task1/ls-l_text.png)

**mkdir text2** command is used to create a new directory in the current working directory. In this case, I create a 
new directory *text2* in directory *text**. As a result, there are file *text.txt* and directory *text2* the working 
directory *text*.

![mkdir](images_task1/mkdir_text2.png)

**mv text.txt text2** command is used to move the file *text.txt* to the directory *text2*.

![mv text.txt text2](images_task1/mv_text.png)

**cd text2** command is used to change the working directory to *text2*.

![cd text2](images_task1/cd_text2.png)

**ls** command is used to check that the file *text.txt* exists in the directory 
*text2*.

![ls text2](images_task1/ls_text2.png)

**mv text.txt text2.txt** command is used to rename existed file *text.txt* to *text2.txt*.
Then I use **ls** command to check that in the directory *text2* there is only file *text2.txt*.

![mv text.txt text2.txt](images_task1/mv_rename.png)

**cp text2.txt ..** command stands for copy. This command is used to copy files or group of files or directory. 
It creates an exact image of a file on a disk with different file name. In this case, I copy the file *text2.txt* 
in the parental directory *test*. After that, I use command **cd ..** to move to the parental directory *text* and 
set it as the current working directory. By using command **ls** I see that there are the copied file *text2.txt* and the 
directory *text*.

![cp](images_task1/cp.png)

In order to delete the file *text2.txt* I use the command **rm text2.txt**. In 
order to delete the directory *text* I use the command **rmdir text**. 

![rm](images_task1/rm.png) 

**1d.** Execute and describe the difference.
* cat /etc/fstab
* less /etc/fstab
* more /etc/fstab

The **/etc/fstab** file is a system configuration file that contains all available disks, disk partitions and their 
options. Each file system is described on a separate line. Each line contains six fields separated by one or more 
spaces or tabs. If you add a new hard disk or have to repartition the existing one, you’ll probably need to modify 
this file.

The **/etc/fstab** file is used by the mount command, which reads the file to determine which options should be used 
when mounting the specified device.

By using the command **cat /etc/fstab** I see the content of this file.
**More**, **less** command allows us to view the contents of a file and navigate through file. 
The main difference between **more** and **less** is that **less** command is faster because it does not load the 
entire file at once and allows navigation though file using page up/down keys. **More** could only scroll down.



**1e.** Look through man pages of the listed above commands.

### Task 2
**2a.** Discovering soft and hard links. Comment on results of these commands 
(place the output into your report):
* cd
* mkdir test
* cd test
* touch test1.txt
* echo “test1.txt” > test1.txt
* ls -l 
* ln test1.txt test2.txt
* ls -l . 
* echo “test2.txt” > test2.txt
* cat test1.txt test2.txt
* rm test1.txt
* ls-l . 
* ln -s test2.txt test3.txt
* ls -l .
* rm test2.txt; ls -l .

**cd** command takes me back to the previous directory.

![cd](./images_task2/cd.png)

By using the command **mkdir test** I create a new directory in the current working directory. In this case, I create 
new directory *text2* in directory *text**. In this case, I attempt to create a new directory *test* in directory *annaimseneckaa*. 
But the directory test has already existed that's why I received the appropriate message.

![mkdir_test](./images_task2/mkdir_test.png)

In order to change the current working directory to the directory *test* I use command **cd test**. After that, I create 
a new file *test1.txt* by using the command **touch test1.txt**. To write some content to the file *test1.txt* I use the 
command **echo "test1.txt" > test1.txt**. As a result, by using the command **ls** I check that the file *test1.txt* exists in the current working 
directory *test* and it isn't empty.

![echo "test1.txt](./images_task2/echo.png)

In order to create a hard link *text2.txt* to the file *text1.txt* I use the command **ln text1.txt text2.txt**. The using 
of a hard link allows multiple file names to be associated with the same file since a hard link points to the inode of a 
given file, the data of which is stored on disk. After that I use the command **ls -l .** to list the files in the working 
directory in long listing format. There are two files *text1.txt* and *text2.txt*. Note that the *text2.txt* is a hard link.

![ln](./images_task2/ln.png)

Next step - I write some content to the file *test2.txt** by using the command **echo “test2.txt” > test2.txt**. In order to see 
the total content of both files *test1.txt* and *test2.txt* I use the command **cat test1.txt test2.txt**. 
Since the file *test2.txt* is a link to a file *test1.txt*, when the file *text2.txt* was changed, the contents of the file 
*test1.txt* were overwritten.

![cat](./images_task2/cat.png)

In order to remove the file *test1.txt* I use the command **rm test1.txt**. After that I use the command **ls -l .** to 
list the files in the working directory in long listing format. There are only the file *test2.txt*.

Next step - to create a soft link *text3.txt* to the file *text2.txt*. In order to do it I use the command 
**ln -s test2.txt test3.txt**. To see the list of the files in the working directory in long listing format 
I use the command **ls -l .** There are two files *text2.txt* and *text3.txt*. Note that the *text3.txt* is a soft 
link to the file *test2.txt*.

At the end I remove the file *test2.txt** by using the command **rm test2.txt**. By using the command **ls -l .** I see 
that there are only the file *test3.txt*.

![soft_link](./images_task2/soft_link.png)

**2b.** Dealing with **chmod**.
An executable script. Open your favourite editor and put these lines into a file.
```bash
#!/bin/bash
echo “Drugs are bad MKAY?”
```
Give name "script.sh" to the script and call to.

In the first I create a new file *script.sh* in the current working directory *test*. By using the editor *nano* 
I write the code into the the file *script.sh*. In order to set execute permission on my script I use the command 
**chmod +x script.sh**. Finally I run this executable script from the bash.

![exec](./images_task2/exec.png)







































